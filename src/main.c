#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "prototype.h"
#include "struct.h"
#define MAX_LETTER 100

/* Cap phat bo nho dong
int main (int argc, char *argv[]){
 int amountFr = 0;
 scanf("%d", &amountFr);
 int *pMemmories = NULL;
 pMemmories = malloc(amountFr * sizeof(int));
 if (pMemmories == NULL) {
    exit(0);
 }
 for(int i=0; i<amountFr; i++) {
    scanf("%d", &pMemmories[i]);

 }
 for(int i=0; i<amountFr; i++) {
    printf("%d\n",pMemmories[i]);
 }
 free(pMemmories);
 return 0;
}
*/

/* Work with FILE
int main() {
    int *pFile = NULL;
    char str[MAX_LETTER] = "";
    pFile = fopen("/home/lounguyen/Documents/VSFirst/textVS/text","r+");
    if(pFile != NULL) {
        fgets(str, MAX_LETTER, pFile);
        printf("%s", str);
        fclose(pFile);
    } else printf("Invalid!");
 return 0;
}
*/

int main(){
   int minutes = 0, hours = 0;
   scanf("%d", &minutes);
   timeTrade(&minutes, &hours);
   printf("%d:%d", hours, minutes);
 return 0;  
}