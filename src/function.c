
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/*
// CREATE RANDOM NUMS
int  random(){
     int secretNums;
     const MAX = 100, MIN = 1;
     srand (time(NULL));
     secretNums = (rand() % (MAX - MIN + 1)) + MIN;
     return secretNums;
}

// GUESS AND CHECK NUMS
void check(int secretNums){
    int guessNums = 0;
    while(guessNums != secretNums){
        printf("What is your guess number?");
        scanf("%d", &guessNums);
        printf("\n");
        if(guessNums > secretNums)
            printf("Less!\n");
            else if(guessNums < secretNums) printf("More!\n");
                 else break;
    }
    printf("Congratulation! You are right..");

}
*/
// CONVERT TIME
void timeTrade(int *pMinutes, int *pHours){
     *pHours = *pMinutes / 60;
     *pMinutes = *pMinutes % 60;
}

//SUM ARR
int sumArr(int arr[], int sizeArr){
    if(sizeArr < 0){
        return 0;
    } else{
        return arr[sizeArr] + sumArr(arr, sizeArr-1);
    }
}

//AVERAGE ARR
int averageArr(int arr[],int sizeArr){
    int sum = 0;
    for(int i=0; i<sizeArr; i++){
        sum += arr[i];
    }
    return sum/sizeArr;
}

//COPY ARR
void copyArray(int array1[], int array2[], int kichThuoc){
    for(int i=0; i<kichThuoc; i++){
        array2[i]=array1[i];
        printf("%d\n", array2[i]);
    }
}

//COMPARE_MAX
void maximumArray(int arr[], int kichThuoc, int giaTriMax){
    for(int i=0; i<kichThuoc; i++){
        if(arr[i] > giaTriMax){
            arr[i] = 0;
        }
    }
}

//ARRNGE ARR
void sapXepArray(int arr[], int kichThuoc){
    int tg;
    for(int i = 0; i < kichThuoc - 1; i++){
        for(int j = i + 1; j < kichThuoc; j++){
            if(arr[i] < arr[j]){
                tg = arr[i];
                arr[i] = arr[j];
                arr[j] = tg;
            }
        }
    }
}
//String
int lengthStr(char str[]){
    int count=0;
    for(int i=0; ; i++){
    if(str[i] != '\0') count+=1; else return count;
    }
}

//Struct
typedef struct Character Character;
struct Character{
    char userName[100];
    char server[100];
    int level;
    int power;
};

//Coordinates
typedef struct Point Point;
struct Point{
    int x;
    int y;
};

void setPoint(Point *pPoint, int a, int b){

    (*pPoint).x = a;
    (*pPoint).y = b;
}

