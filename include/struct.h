typedef struct Character Character;
struct Character{
    char userName[100];
    char server[100];
    int level;
    int power;
};

typedef struct Point Point;
struct Point{
    int x;
    int y;
};
